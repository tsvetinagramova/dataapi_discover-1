#!/home/tariq/DataAPI/dataapienv/bin/python
# coding: utf-8

# In[1]:


########This code is to update the footfall calculations by space that will be used in discover


# In[2]:


##########Path Locations and Variables


# In[3]:


import json
#get_ipython().run_line_magic('matplotlib', 'inline')
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import csv
import geopandas as gpd
from shapely.geometry import shape
from shapely.geometry import Point
import re
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import numpy as np
import json
import geog
import shapely.geometry
from shapely.geometry import Point, LineString
from geopandas import GeoDataFrame
import geopandas as gp
from shapely.geometry import Point, Polygon
import datetime
import pickle
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from pandas.io.json import json_normalize
from datetime import datetime


# In[4]:


#Impoort of X-Mode Footfall Data
xmode_footfall = pd.read_csv('geo_xmode_july.csv')


# In[5]:


#Convert Data and put into GeoDataFrame
xmode_footfall['Coordinates'] = list(zip(xmode_footfall.longitude, xmode_footfall.latitde))
xmode_footfall['Coordinates'] = xmode_footfall['Coordinates'].apply(Point)
gdf = gpd.GeoDataFrame(xmode_footfall, geometry='Coordinates')
gdf.shape


# In[6]:


#CREATE INDEX TO MAKE SEARCHING MORE EFFICIENT
spatial_index=gdf.sindex 


# In[7]:


############# Import Spaces Dataset and filer for London ##############

import urllib.request
import requests
import ssl

pd.options.mode.chained_assignment = None

ssl._create_default_https_context = ssl._create_unverified_context

url = "https://stagingpopapi.northeurope.cloudapp.azure.com/new-spaces/search/spaces/webscraper/geoviewport?south=51.38494009999999&west=-0.351468299999965&north=51.6723432&east=0.14827100000002247&requestedFields=address,rates,title,titleName,dimensions,featuredImage,scraper"
hdr = { 'User-Agent':'PostmanRuntime/7.6.0','Authorization' : 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IlNha2FjaWMiLCJyb2xlcyI6WyJQUk8iLCJCRVRBIiwiQURNSU4iLCJCQVNJQyJdLCJ1c2VyQWdlbnQiOiJQb3N0bWFuUnVudGltZS83LjYuMCIsInBybyI6dHJ1ZSwidXNlcklkIjo4OTUsInN1c3BlbmRlZCI6ZmFsc2UsImZpcnN0TmFtZSI6IlNsYXZlbiIsImFjY2VwdGVkVG9zIjp0cnVlLCJyZW1lbWJlck1lIjpmYWxzZSwiZXhwIjoxODc1MDQ5NDAxLCJpYXQiOjE1NTE3ODk0NzksImJldGEiOmZhbHNlLCJlbWFpbCI6InNsYXZlbkBwb3BlcnRlZS5jb20ifQ.Fy_gCLB6Tc8NeGJhw-s8LsnbDq71d1bbjITcuZegHBEUrEecAH8Y9thPlTTPoo8QPQbuQPSKEVt-H6iMwN3LYQ' }

req = urllib.request.Request(url, headers=hdr)
response = urllib.request.urlopen(req)
space_call = json.load(response)
    
space_json = space_call['spaces']

space_data = json_normalize(space_json)
space_data['address.geolocation.lng'] = space_data['address.geolocation.coordinates'].str[0]
space_data['address.geolocation.lat'] = space_data['address.geolocation.coordinates'].str[1]

locations_test = space_data[['_id','address.geolocation.lat','address.geolocation.lng','address.city','address.citySector','address.country']]
locations_test['testloc'] = locations_test['_id']
locations_test['latitude'] = locations_test['address.geolocation.lat']
locations_test['longitude'] = locations_test['address.geolocation.lng']
locations_test['city'] = locations_test['address.city']
locations_test['citySector'] = locations_test['address.citySector']
locations_test['Country'] = locations_test['address.country']
locations_test = locations_test[['testloc','latitude','longitude','city','citySector','Country']]

city = locations_test.city.isin(['London'])
citysec = locations_test.citySector.isin(['London'])

locations_test_ = locations_test[city | citysec].reset_index()
locations_test_2 = locations_test_[['testloc','latitude','longitude']]

testloc = locations_test_2.copy()


# In[8]:


#Convert Space Data and put into GeoDataFrame
testloc['Coordinates'] = list(zip(testloc.longitude, testloc.latitude))
testloc['Coordinates'] = testloc['Coordinates'].apply(Point)
location = gpd.GeoDataFrame(testloc, geometry='Coordinates')
location.shape


# In[9]:


total_rows = len(location.index)
#Interates through test spaces, creates 100m boundary 
for x in range(total_rows): 
    firstloc = location.iloc[[x]]
    n_points = 20
    d = 1 * 100  # meters
    angles = np.linspace(0, 360, n_points)
    polygon = geog.propagate(firstloc['Coordinates'][x], angles, d)
    if x==0:
        spaces_polygon = pd.DataFrame(polygon)
        spaces_polygon.columns = ['longitude','latitude']
        spaces_polygon['location'] = location['testloc'][x]
    else:
        spaces_polygon_n = pd.DataFrame(polygon)
        spaces_polygon_n.columns = ['longitude','latitude']
        spaces_polygon_n['location'] = location['testloc'][x]
        spaces_polygon=spaces_polygon.append(spaces_polygon_n, ignore_index=True)


# In[10]:


#Convert Data and put into GeoDataFrame
geometry = [Point(xy) for xy in zip(spaces_polygon.longitude, spaces_polygon.latitude)]
spaces_polygon = GeoDataFrame(spaces_polygon, geometry=geometry)
#spaces_polygon


# In[11]:


#Convert point data into Poloygon
spaces_polygon = spaces_polygon.drop(['longitude','latitude'], axis=1)
spaces_polygon['geometry'] = spaces_polygon['geometry'].apply(lambda y: y.coords[0])
spaces_polygon = spaces_polygon.groupby('location')['geometry'].apply(lambda y: Polygon(y.tolist())).reset_index()
spaces_polygon = gp.GeoDataFrame(spaces_polygon, geometry = 'geometry')


# In[12]:


# matches Xmode location data with Space Polygon to get footfall
for x in range(total_rows):   
    indpost=spaces_polygon['geometry'].iloc[x]
    indpost_bb = indpost.bounds
    possible_matches_index = list(spatial_index.intersection(indpost_bb))
    possible_matches = gdf.iloc[possible_matches_index]
    possible_matches.shape
    precise_matches = possible_matches[possible_matches.intersects(indpost)]
    precise_matches.shape
    if x==0:
        space_match = precise_matches.copy()
        space_match['space_name']=str(spaces_polygon['location'][x])
    else:
        space_match_new=precise_matches.copy()
        space_match_new['space_name']=str(spaces_polygon['location'][x])
        space_match=space_match.append(space_match_new, ignore_index=True)


# In[13]:


#copy space match data to new DF
df = space_match.copy()


# In[14]:


###############Covert date field to readable format##################
def unixconv(datefield):
    datefield = str(datetime.fromtimestamp(datefield).strftime('%Y-%m-%d %H:%M:%S'))
    return str(datefield)

def hourextract(ep):
    hour_ext = str(datetime.fromtimestamp(ep).strftime(('%H')))
    return str(hour_ext)

def weekday(datefield):
    weekday_no = str(datetime.fromtimestamp(datefield).weekday())
    return str(weekday_no)


# In[15]:


#Covert unix timestamp to readable format
df['timestamp'] = df['timestamp'] + (1*60*60)
df['datetime'] = df['timestamp'].apply(unixconv)
#################   Extracting the hour of the day   #######################
df['hour_extract'] = df['timestamp'].apply(hourextract)
df['hour_of_day']=df['hour_extract'].astype(str).astype(int)
df['hour_of_day'] += 1
#################   Extracting the weekday #######################
df['weekday_no'] = df['timestamp'].apply(weekday)
########## Removal of Not Neccessary Columns ##############
df = df.drop(df.columns[[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]], axis=1)


# In[16]:


spaces_countu = df.groupby(['space_name','weekday_no','hour_of_day']).advertiser_id.nunique()
spaces_count = df.groupby(['space_name','weekday_no','hour_of_day']).agg('count')


# In[17]:


#Sum by defined groups
spaces_countu = df.groupby(['space_name','weekday_no','hour_of_day']).advertiser_id.nunique()
spaces_count = df.groupby(['space_name','weekday_no','hour_of_day']).agg('count')

#Export Data
spaces_countu.to_csv('spaces_countu.csv', sep='\t',header=False)
spaces_count.to_csv('spaces_count.csv', sep='\t',header=False)

############# Import Data Files 
xm_u = pd.read_csv('spaces_countu.csv', sep='\t', names=(['space_name','weekday_no', 'hour_of_day','volume_xmu']))
xm = pd.read_csv('spaces_count.csv', sep='\t', names=(['space_name','weekday_no', 'hour_of_day','volume_xm']))


# In[18]:


def hourgrp(thourf):
    if (thourf >=6 and thourf<=12):
        h='Morning'
    elif (thourf >=13 and thourf<=17):
        h='Afternoon'
    elif (thourf >=18 and thourf<=21):
        h='Evening'
    else:
        h='Night'
    return str(h)

def hourex(timefield):
    t, d = timefield.split(".", 1)
    return str(t)


# In[19]:


#Combine Data to get unique and total count
xm_m = xm.groupby(['space_name','weekday_no', 'hour_of_day']).sum()
xm_m.to_csv('xm_m.csv', sep='\t')
xm_ming = pd.read_csv('xm_m.csv', sep='\t')


xm_umm = xm_u.groupby(['space_name','weekday_no', 'hour_of_day']).sum()
xm_umm.to_csv('xm_um.csv', sep='\t')
xm_uming = pd.read_csv('xm_um.csv', sep='\t')

xm_mcing = pd.merge(xm_ming,
                xm_uming[['space_name','weekday_no','hour_of_day','volume_xmu']],
                       left_on=['space_name','weekday_no','hour_of_day'],
                       right_on=['space_name','weekday_no','hour_of_day'],
                       how='left')


# In[20]:


#Drop missing fields and create hour groups
xm_td_model = xm_mcing.dropna(axis=0, subset=['volume_xmu'])
xm_td_model['hour_of_day']=xm_td_model['hour_of_day'].apply(hourgrp)


# In[21]:


#Redo summations of data
xm_td_model = xm_td_model.groupby(['space_name','weekday_no', 'hour_of_day']).sum()
xm_td_model.to_csv('xm_td_model', sep='\t')
xm_td_model_fin = pd.read_csv('xm_td_model', sep='\t')


# In[22]:


#Recode labels to prepare to standardization
daypart_ord_map = {'Morning':1, 'Afternoon':2, 'Evening':3, 'Night':4}
xm_td_model_fin['daypart_label'] = xm_td_model_fin['hour_of_day'].map(daypart_ord_map)
weekday_ord_map = {0:'Monday', 1:'Tuesday', 2:'Wednesday', 3:'Thursday', 4:'Friday', 5:'Saturday', 6:'Sunday'}
xm_td_model_fin['Weekday_label'] = xm_td_model_fin['weekday_no'].map(weekday_ord_map)


# In[23]:


day_le = LabelEncoder()
day_labels = day_le.fit_transform(xm_td_model_fin['hour_of_day'])
xm_td_model_fin['daypart_label_'] = day_labels

day_ohe = OneHotEncoder()
day_feature_arr = day_ohe.fit_transform(
                              xm_td_model_fin[['daypart_label_']]).toarray()
day_feature_labels = list(day_le.classes_)
day_features = pd.DataFrame(day_feature_arr, 
                            columns=day_feature_labels)

weekday_le = LabelEncoder()
weekday_labels = weekday_le.fit_transform(xm_td_model_fin['Weekday_label'])
xm_td_model_fin['weekday_no_'] = weekday_labels

weekday_ohe = OneHotEncoder()
weekday_feature_arr = weekday_ohe.fit_transform(
                              xm_td_model_fin[['weekday_no_']]).toarray()
weekday_feature_labels = list(weekday_le.classes_)
weekday_features = pd.DataFrame(weekday_feature_arr, 
                            columns=weekday_feature_labels)

mod = pd.concat([xm_td_model_fin, day_features, weekday_features], axis=1)
mod_data = mod.drop(mod.columns[[1, 2, 5, 6, 7, 8]], axis=1)


# In[24]:


#Get Max and Min values to normalize data for model scoring
xm_u_j = pd.read_csv('xmode_mdata', sep='\t', names=(['postsect','weekday_no', 'hour_of_day','volume_xmu']))
xm_j = pd.read_csv('xmode_mdata_ct', sep='\t', names=(['postsect','weekday_no', 'hour_of_day','volume_xm']))

xm_m_j = xm_j.groupby(['postsect','weekday_no', 'hour_of_day']).sum()
xm_m_j.to_csv('xm_m_j.csv', sep='\t')
xm_ming_j = pd.read_csv('xm_m_j.csv', sep='\t')

xm_umm_j = xm_u_j.groupby(['postsect','weekday_no', 'hour_of_day']).sum()
xm_umm_j.to_csv('xm_umm_j.csv', sep='\t')
xm_uming_j = pd.read_csv('xm_umm_j.csv', sep='\t')

xm_mcing_j = pd.merge(xm_ming_j,
                xm_uming_j[['postsect','weekday_no','hour_of_day','volume_xmu']],
                       left_on=['postsect','weekday_no','hour_of_day'],
                       right_on=['postsect','weekday_no','hour_of_day'],
                       how='left')

xm_mcing_j['hour_of_day']=xm_mcing_j['hour_of_day'].apply(hourgrp)
xm_mcing_j = xm_mcing_j.groupby(['postsect','weekday_no', 'hour_of_day']).sum()
xm_mcing_j.to_csv('xm_mcing_j', sep='\t')
xm_mcing_j = pd.read_csv('xm_mcing_j', sep='\t')


# In[25]:


#Apply normalization to data
mod_data['volume_xm']=((mod_data['volume_xm']-min(xm_mcing_j['volume_xm']))/(max(xm_mcing_j['volume_xm'])-min(xm_mcing_j['volume_xm'])))
mod_data['volume_xmu']=((mod_data['volume_xmu']-min(xm_mcing_j['volume_xmu']))/(max(xm_mcing_j['volume_xmu'])-min(xm_mcing_j['volume_xmu'])))


# In[26]:


#Calculate square kilometer of postcodes
r = json.loads(open('london_geo_only_correct.json').read())
n = r["features"]
#n[1]

for d in n:
    d['geometry'] = shape(d['geometry'])

hennepin = gpd.GeoDataFrame(n).set_geometry('geometry')


crs = {'init': 'epsg:4326'}
hennepin = gpd.GeoDataFrame(hennepin, crs=crs)

hennepin = hennepin.to_crs({'init': 'epsg:3857'})

hennepin["area"] = hennepin['geometry'].area/ 10**6

hennepin = hennepin.to_crs({'init': 'epsg:4326'})
#hennepin.head(20)


# In[27]:


#Function to format the PC_sector field correctly
def clean(post_name):
    post_name = re.search("{'name': '(.+?)'}", post_name).group(1)
    return str(post_name)


# In[28]:


#Convert Test Data and put into GeoDataFrame and indexes it for searching
#testloc = pd.read_csv('/Users/tariqzaki/Geospatial Analysis/test_cords.csv')
testloc = locations_test_2.copy()
testloc['Coordinates'] = list(zip(testloc.longitude, testloc.latitude))
testloc['Coordinates'] = testloc['Coordinates'].apply(Point)
location = gpd.GeoDataFrame(testloc, geometry='Coordinates')
location.shape
spatial_index_2=location.sindex


# In[29]:


#finds in which postcodes the spaces are in
for x in range(1316):   
    indpost=hennepin['geometry'].iloc[x]
    indpost_bb = indpost.bounds
    possible_matches_index = list(spatial_index_2.intersection(indpost_bb))
    possible_matches = location.iloc[possible_matches_index]
    possible_matches.shape
    precise_matches = possible_matches[possible_matches.intersects(indpost)]
    precise_matches.shape
    if x==0:
        geo_enriched_pc = precise_matches.copy()
        geo_enriched_pc['PC_sector']=str(hennepin['properties'][x])
        geo_enriched_pc['area']=hennepin['area'][x]
    else:
        geo_enriched_next=precise_matches.copy()
        geo_enriched_next['PC_sector']=str(hennepin['properties'][x])
        geo_enriched_next['area']=hennepin['area'][x]
        geo_enriched_pc=geo_enriched_pc.append(geo_enriched_next, ignore_index=True)


# In[30]:


geo_enriched_pc['PC_sector'] = geo_enriched_pc['PC_sector'].apply(clean)


# In[31]:


geo_enriched_pc = geo_enriched_pc.drop(['latitude','longitude'], axis=1)


# In[32]:


Space_dfor_M = pd.merge(mod_data,
                geo_enriched_pc[['testloc','PC_sector','area']],
                       left_on=['space_name'],
                       right_on=['testloc'],
                       how='left')

Space_dfor_M = Space_dfor_M.drop(['testloc'], axis=1)


# In[33]:


#copy data and remove fields that aren't required for scoring
scoredata = Space_dfor_M.copy()
scoredata = scoredata[['volume_xmu','Afternoon','Evening','Morning','Night','Friday','Monday','Saturday','Sunday','Thursday','Tuesday','Wednesday']]
#scoredata = scoredata[['volume_xmu']]


# In[34]:


#Load and run model
poly_reg = PolynomialFeatures(degree = 3) 
regressor = LinearRegression() 
filename = 'poly_model.sav'
loaded_model = pickle.load(open(filename, 'rb'))
y_pred = loaded_model.predict(poly_reg.fit_transform(scoredata)) 


# In[35]:


#Select and combine Data for API using index
predicted_footfall = pd.DataFrame(y_pred, columns = ["predval"])
scored_data = Space_dfor_M.join(predicted_footfall)
scored_data_it = scored_data[['space_name','PC_sector','predval']]
reconfig_d = xm_td_model_fin[['hour_of_day','volume_xmu','Weekday_label']]
api_data = scored_data_it.join(reconfig_d)
weekday_ord_map = {'Monday':'0', 'Tuesday':'1', 'Wednesday':'2', 'Thursday':'3', 'Friday':'4', 'Saturday':'5', 'Sunday':'6'}
api_data['Weekday_label'] = api_data['Weekday_label'].map(weekday_ord_map)


# In[36]:


##########################################Step4 - Sense Check Data##################################################


# In[37]:


td_ul = pd.read_csv('tele_jan.csv')

tdul=td_ul.copy()
ps_info=td_ul.copy()

ps_info = ps_info.groupby(['postsect']).sum().reset_index()
ps_info['ps_footfall']=ps_info['volume']
ps_info=ps_info[['postsect','ps_footfall']]


# In[38]:


#Create DF
tdul = tdul[['postsect','day_of_week','time_slot','volume']]


# In[39]:


#Inital Aggregation
tdul = tdul.groupby(['postsect','day_of_week','time_slot']).sum().reset_index()
#tdul.to_csv('tdul.csv', sep='\t')
#tdul = pd.read_csv('/Users/tariqzaki/Geospatial Analysis/tdul.csv', sep='\t')#Create new fields and labels
weekday_ord_map = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6'}
tdul['Weekday_label'] = tdul['day_of_week'].map(weekday_ord_map)
tdul['hour_of_day']=tdul['time_slot'].apply(hourgrp)
tdul = tdul.drop(['day_of_week','time_slot'], axis=1)


# In[40]:


tdul = tdul.groupby(['postsect','hour_of_day','Weekday_label']).sum().reset_index()
#tdul.to_csv('tdul.csv', sep='\t')
#tdul = pd.read_csv('/Users/tariqzaki/Geospatial Analysis/tdul.csv', sep='\t')


# In[41]:


#Strip White Space in field
tdul["postsect"] = tdul["postsect"].str.strip()


# In[42]:


xm_comp=xm_u_j.copy()


# In[43]:


#Create new fields and labels

xm_comp['Weekday_label'] = xm_comp['weekday_no'].map(weekday_ord_map)
xm_comp['hour_of_day']=xm_comp['hour_of_day'].apply(hourgrp)
xm_comp['volume_psu']=xm_comp['volume_xmu']
xm_comp = xm_comp.drop(['weekday_no','volume_xmu'], axis=1)


# In[44]:


xm_comp = xm_comp.groupby(['postsect','hour_of_day','Weekday_label']).sum().reset_index()
#xm_comp.to_csv('xm_comp.csv', sep='\t')
#xm_comp = pd.read_csv('/Users/tariqzaki/Geospatial Analysis/xm_comp.csv', sep='\t')


# In[45]:


#Merge Data
api_data_u = pd.merge(api_data,
                tdul[['postsect','hour_of_day','Weekday_label','volume']],
                       left_on=['PC_sector','hour_of_day','Weekday_label'],
                       right_on=['postsect','hour_of_day','Weekday_label'],
                       how='left')


# In[46]:


#Merge Data
api_data_ul = pd.merge(api_data_u,
                xm_comp[['postsect','hour_of_day','Weekday_label','volume_psu']],
                       left_on=['postsect','hour_of_day','Weekday_label'],
                       right_on=['postsect','hour_of_day','Weekday_label'],
                       how='left')

api_data_ul = api_data_ul.drop(['PC_sector'], axis=1)


# In[47]:


api_data_ul = api_data_ul[['space_name','postsect','Weekday_label','hour_of_day','volume','volume_psu','volume_xmu','predval']]


# In[48]:


#Creat sense check bounds
api_data_ul["expected"] = api_data_ul['volume_xmu']*(api_data_ul['volume']/api_data_ul['volume_psu'])
api_data_ul["upper_v"] = api_data_ul["expected"].apply(lambda x:x*1.2)
api_data_ul["lower_v"] = api_data_ul["expected"].apply(lambda x:x*.8)
api_data_ul["footfall"] = api_data_ul["predval"]


# In[49]:


#Perform Sense Check
total_rows = len(api_data_ul.index)

for x in range(total_rows):
    if (api_data_ul['predval'][x]>=api_data_ul["lower_v"][x] and api_data_ul['predval'][x]<= api_data_ul["upper_v"][x]):
        api_data_ul['footfall'][x]=api_data_ul['predval'][x]
    elif (api_data_ul['predval'][x] > api_data_ul["upper_v"][x]):
        api_data_ul['footfall'][x]=api_data_ul["upper_v"][x]
    else:
        api_data_ul['footfall'][x]=api_data_ul["lower_v"][x]        


# In[50]:


###########################################Percentage Breakdown Calc################################################


# In[51]:


#Drop unneccesary variables
Demo_calc = api_data_ul.drop(['volume','volume_psu','volume_xmu','predval','expected','upper_v','lower_v'], axis=1)


# In[52]:


#Read in precentages csv
percentages = pd.read_csv('percentages.csv', sep='\t')
weekday_ord_map = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6'}
percentages['day_of_week'] = percentages['day_of_week'].map(weekday_ord_map)


# In[53]:


#Merge demo and model data
Fin_data = pd.merge(Demo_calc,
                percentages[['postsect','time_slot','day_of_week','visitor_type','gender','age','affluence','volume_per']],
                       left_on=['postsect','hour_of_day','Weekday_label'],
                       right_on=['postsect','time_slot','day_of_week'],
                       how='left')

Fin_data = Fin_data.drop(['time_slot','day_of_week'], axis=1)


# In[54]:


#Calculate footfall for the different groups 
Fin_data["volume"] = round(Fin_data['footfall']*Fin_data['volume_per'])
#Fin_data["day_part"] = Fin_data['hour_of_day']
#Fin_data = Fin_data.drop(['hour_of_day'], axis=1)


# In[55]:


####### Recode variables into numeric form for Data Call

daypart_ord_map = {'Morning':1, 'Afternoon':2, 'Evening':3, 'Night':4}
Fin_data['day_part'] = Fin_data['hour_of_day'].map(daypart_ord_map)

daypart_affluence_map = {1:0,2:1,3:2,4:3,5:4}
Fin_data['affluence'] = Fin_data['affluence'].map(daypart_affluence_map)

age_ord_map = {'18-24':0, '25-34':1, '35-44':2, '45-54':3, '55-64':4, '65+':5}
Fin_data['age'] = Fin_data['age'].map(age_ord_map)
Fin_data = Fin_data.drop(['hour_of_day'], axis=1)


# In[56]:


ps_info["postsect"] = ps_info["postsect"].str.strip()

fin_data = pd.merge(Fin_data,
                ps_info[['postsect','ps_footfall']],
                       left_on=['postsect'],
                       right_on=['postsect'],
                       how='left')


# In[61]:


fin_data = fin_data.dropna()
fin_data.to_csv('/home/tariq/DataAPI/spc_scrap_data.csv', sep='\t', index=False)

#scrap_data = fin_data.dropna()
#scrap_data.to_csv('/home/tariq/DataAPI/scrap_data.csv', sep='\t', index=False)


